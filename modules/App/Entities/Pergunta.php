<?php

namespace App\Entities;

class Pergunta
{
    private $isResposta = false;
    private $pergunta = null;
    private $resposta = null;
    private $proximaCasoNao = null;
    private $proximaCasoSim = null;
    private $perguntaAnterior = null;

    /**
     *
     * @param boolean|null $flag
     * @return boolean
     */
    public function isResposta($flag=null)
    {
        if($flag !== null) {
            $this->isResposta = !!$flag;
        }
        return $this->isResposta;
    }

    /**
     *
     * @return string
     */
    public function getPergunta()
    {
        return $this->pergunta;
    }

    /**
     *
     * @param string $pergunta
     * @return \App\Entities\Pergunta
     */
    public function setPergunta($pergunta)
    {
        $this->pergunta = $pergunta;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getResposta()
    {
        return $this->resposta;
    }

    /**
     *
     * @param string $resposta
     * @return \App\Entities\Pergunta
     */
    public function setResposta($resposta)
    {
        $this->isResposta(true);
        $this->resposta = $resposta;
        return $this;
    }

    /**
     *
     * @return \App\Entities\Pergunta
     */
    public function getProximaPerguntaCasoNao()
    {
        return $this->proximaCasoNao;
    }

    /**
     *
     * @param Pergunta $pergunta
     * @return \App\Entities\Pergunta
     */
    public function setProximaPerguntaCasoNao(Pergunta $pergunta=null)
    {
        if($pergunta)
        {
            $pergunta->setPerguntaAnterior($this);
        }
        $this->proximaCasoNao = $pergunta;
        return $this;
    }

    /**
     *
     * @return \App\Entities\Pergunta
     */
    public function getProximaPerguntaCasoSim()
    {
        return $this->proximaCasoSim;
    }

    /**
     *
     * @param Pergunta $pergunta
     * @return \App\Entities\Pergunta
     */
    public function setProximaPerguntaCasoSim(Pergunta $pergunta)
    {
        $pergunta->setPerguntaAnterior($this);
        $this->proximaCasoSim = $pergunta;
        return $this;
    }

    /**
     *
     * @param Pergunta $pergunta
     * @return \App\Entities\Pergunta
     */
    public function setPerguntaAnterior(Pergunta $pergunta)
    {
        $this->perguntaAnterior = $pergunta;
        return $this;
    }

    /**
     *
     * @return \App\Entities\Pergunta
     */
    public function getPerguntaAnterior()
    {
        return $this->perguntaAnterior;
    }

    /**
     *
     * @return \App\Entities\Pergunta
     */
    public function getUltimaPerguntaQueNaoResposta()
    {
        $pergunta = $this;
        while($pergunta instanceof Pergunta && $pergunta->isResposta()) {
            $pergunta = $pergunta->getPerguntaAnterior();
        }
        return $pergunta;
    }

}