<?php

namespace App\Model;

use App\Entities\Pergunta as Pergunta;

class PerguntasContainer {

    const RESPOSTA_SIM = 'sim';
    const RESPOSTA_NAO = 'nao';

    /**
     *
     * @var \App\Entities\Pergunta
     */
    private $container;
    /**
     *
     * @var \App\Entities\Pergunta
     */
    private $perguntaAtual;

    /**
     *
     * @var boolean
     */
    private $encontrouResposta = false;
    /**
     *
     * @var boolean
     */
    private $necessitaMaisInformacoes = false;

    public function __construct()
    {
        $this->container = $this->_createEstruturaBase();
        $this->perguntaAtual = $this->container;
    }

    /**
     *
     * @return \App\Entities\Pergunta;
     */
    private function _createEstruturaBase()
    {
        $tubarao = new Pergunta();
        $tubarao->isResposta(true);
        $tubarao->setPergunta('é Tubarão');
        $tubarao->setResposta('Tubarão');

        $macaco = new Pergunta();
        $macaco->isResposta(true);
        $macaco->setPergunta('é Macaco');
        $macaco->setResposta('Macaco');

        $pergunta = new Pergunta();
        $pergunta->isResposta(false);
        $pergunta->setPergunta('vive na água');
        $pergunta->setProximaPerguntaCasoNao($macaco);
        $pergunta->setProximaPerguntaCasoSim($tubarao);
        return $pergunta;
    }

    /**
     *
     * @return \App\Entities\Pergunta
     */
    public function getPerguntaAtual()
    {
        return $this->perguntaAtual;
    }

    /**
     *
     * @return boolean
     */
    public function isRespostaEncontrada()
    {
        return $this->encontrouResposta;
    }

    /**
     *
     * @return boolean
     */
    public function isNecessarioMaisInformacoes()
    {
        return $this->necessitaMaisInformacoes;
    }

    /**
     *
     * @return \App\Model\PerguntasContainer
     */
    public function reset()
    {
        $this->perguntaAtual = $this->container;
        $this->encontrouResposta = false;
        $this->necessitaMaisInformacoes = false;
        return $this;
    }

    public function proximaPergunta($resposta)
    {
        if(!in_array($resposta, array(self::RESPOSTA_SIM, self::RESPOSTA_NAO))) {
            throw new \Exception('Resposta Invalida!');
        }
        $perguntaAtual = $this->getPerguntaAtual();
        if($resposta == self::RESPOSTA_SIM) {
            if($perguntaAtual->isResposta()) {
                $this->encontrouResposta = true;
                return $this;
            }
            $proximaPergunta = $perguntaAtual->getProximaPerguntaCasoSim();
        }
        else {
            $proximaPergunta = $perguntaAtual->getProximaPerguntaCasoNao();
            if($proximaPergunta == null)
            {
                $this->necessitaMaisInformacoes = true;
                return $this;
            }
        }
        $this->perguntaAtual = $proximaPergunta;
        return $this;
    }

    public function append(Pergunta $pergunta)
    {
        $this->getPerguntaAtual()->setProximaPerguntaCasoNao($pergunta);
        return $this;
    }

    public function addNovaDica(Pergunta $pergunta)
    {
        $atual = $this->getPerguntaAtual();
        $ultimaPergunta = $this->getUltimaPergunta();
        $pergunta->setProximaPerguntaCasoNao($atual);
        $pergunta->setProximaPerguntaCasoSim($atual->getProximaPerguntaCasoNao());
        $atual->setProximaPerguntaCasoNao(null);
        if($ultimaPergunta->getProximaPerguntaCasoNao() == $atual)
        {
            $ultimaPergunta->setProximaPerguntaCasoNao($pergunta);
        }
        else {
            $ultimaPergunta->setProximaPerguntaCasoSim($pergunta);
        }
        return $this;
    }

    /**
     *
     * @return \App\Entities\Pergunta
     */
    public function getUltimaPergunta()
    {
        return $this->getPerguntaAtual()->getUltimaPerguntaQueNaoResposta();
    }

}