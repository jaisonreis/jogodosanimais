<?php

namespace App;

use Symfony\Component\HttpFoundation\Request;

use App\Entities\Pergunta;
use App\Model\PerguntasContainer;

class IndexController
{
    /**
     *
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     *
     * @var \Silex\Application
     */
    private $app;

    public function __construct(\Silex\Application $app)
    {
        $this->request = $app['request'];
        $this->app = $app;
    }

    /**
     *
     * @return \Symfony\Component\HttpFoundation\Session\Session
     */
    protected function getSession()
    {
        return $this->app['session'];
    }

    /**
     * @return \Twig_Environment
     */
    protected function getView()
    {
        return $this->app['twig'];
    }

    protected function redirect($uri)
    {
        return $this->app->redirect($uri);
    }

    /**
     *
     * @return \App\Model\PerguntasContainer
     */
    protected function getPerguntasContainer()
    {
        $perguntas = $this->getSession()->get('perguntas.container', false);
        if($perguntas === false) {
            $perguntas = new PerguntasContainer();
            $this->getSession()->set('perguntas.container', $perguntas);
        }
        return $perguntas;
    }

    public function index()
    {
        $this->getPerguntasContainer()->reset();
        return $this->getView()->render('index.twig');
    }

    public function pergunta($answer=null)
    {
        if($this->getPerguntasContainer()->isRespostaEncontrada())
        {
            return $this->redirect('/sucesso');
        }

        if($this->getPerguntasContainer()->isNecessarioMaisInformacoes())
        {
            return $this->redirect('/nova-resposta');
        }

        if($answer)
        {
            $this->getPerguntasContainer()->proximaPergunta($answer);
            if($this->getPerguntasContainer()->isRespostaEncontrada())
            {
                return $this->redirect('/sucesso');
            }
            if($this->getPerguntasContainer()->isNecessarioMaisInformacoes())
            {
                return $this->redirect('/nova-resposta');
            }
        }

        $pergunta = $this->getPerguntasContainer()->getPerguntaAtual();

        return $this->getView()->render('pergunta.twig', array(
            'question' => $pergunta->getPergunta()
        ));
    }

    public function sucesso()
    {
        if(!$this->getPerguntasContainer()->isRespostaEncontrada())
        {
            return $this->redirect('/');
        }
        return $this->getView()->render('sucesso.twig');
    }

    public function novaResposta()
    {
        if(!$this->getPerguntasContainer()->isNecessarioMaisInformacoes())
        {
            return $this->redirect('/');
        }

        if($this->request->isMethod('post'))
        {
            $isCancelar = !!$this->request->get('is-cancelar', false);
            if(!$isCancelar) {
                $pergunta = new Pergunta();
                $pergunta->setPergunta('é ' . $this->request->get('nome-animal'));
                $pergunta->setResposta($this->request->get('nome-animal'));
                $this->getPerguntasContainer()->append($pergunta);
            }
            else {
                return $this->redirect('/');
            }
            return $this->redirect('/nova-dica');
        }

        return $this->getView()->render('nova-resposta.twig');
    }

    public function novaDica()
    {
        if(!$this->getPerguntasContainer()->isNecessarioMaisInformacoes())
        {
            return $this->redirect('/');
        }

        if($this->request->isMethod('post'))
        {
            $isCancelar = !!$this->request->get('is-cancelar', false);
            if(!$isCancelar) {
                $pergunta = new Pergunta();
                $pergunta->setPergunta($this->request->get('dica'));
                $this->getPerguntasContainer()->addNovaDica($pergunta);
            }
            return $this->redirect('/');
        }

        $respostaAtual = $this->getPerguntasContainer()
                                ->getPerguntaAtual()
                                ->getProximaPerguntaCasoNao();

        $ultimaResposta = $this->getPerguntasContainer()
                                ->getPerguntaAtual();

        return $this->getView()->render('nova-dica.twig', array(
            'resposta' => $respostaAtual->getResposta(),
            'ultimaResposta' => $ultimaResposta->getResposta()
            ));
    }

    public function clear()
    {
        $this->getSession()->remove('perguntas.container');
        return $this->redirect('/');
    }

}