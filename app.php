<?php

$autoloader = require_once __DIR__.'/vendor/autoload.php';
$autoloader->add('App', __DIR__ . '/modules');

$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider(), array(
    'session.storage.options' => array(
         'name' => 'animaissess'
    )
));
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

$app->before(function () use ($app) {
    $app['twig']->addGlobal('layout', $app['twig']->loadTemplate('layout.twig'));
});

$app['home.controller'] = $app->share(function(Silex\Application $app) {
    return new App\IndexController($app);
});

$app->get('/', 'home.controller:index');
$app->get('/pergunta', 'home.controller:pergunta');
$app->get('/pergunta/{answer}', 'home.controller:pergunta')->assert('answer','(sim|nao)');
$app->match('/nova-resposta', 'home.controller:novaResposta')->method('GET|POST');
$app->match('/nova-dica', 'home.controller:novaDica')->method('GET|POST');
$app->get('/sucesso', 'home.controller:sucesso');
$app->get('/clear', 'home.controller:clear');

return $app;