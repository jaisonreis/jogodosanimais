Jogo dos Animais
----------------

1. Requisitos
==========

* PHP 5.3.3 >=
* Apache 2.x >

2. Configuração
===============

1. Copiar o conteúdo para uma pasta do servidor
2. Configurar o Apache para utilizar um vhost tendo como `DocumentRoot` a pasta `/web` deste
   conjunto de arquivos
3. Configurar o `/etc/hosts` para apontar o endereço configurado no vhost para `127.0.0.1`
4. Dentro do diretório raiz do repositório de arquivos, executar o comando `php composer.phar install`
   (este comando irá baixar as dependências deste programa como o silex, twig, etc).
5. Acessar o endereço especificado no vhost.

Caso não seja possível configurar o vhost no apache e for necessário colocar o sistema para rodar
em uma pasta dentro do DocumentRoot do apache, alterar o arquivo web/.htaccess, na diretiva 
RewriteBase para apontar o caminho base desta URL.

 * * *

#### Exemplo

* `DocumentRoot = /var/www`
* Raiz do repositorio = ``/var/www/jogodosanimais``
* URL = `http://localhost/jogodosanimais/web`
* Diretiva de configuração (.htaccess) = `RewriteBase /jogodosanimais/web/`